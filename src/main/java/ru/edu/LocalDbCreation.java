package ru.edu;

import java.sql.DriverManager;
import java.sql.SQLException;

public class LocalDbCreation {

    public static final String JDBC_H_2_LOCAL_DB_DB = "jdbc:h2:./target/localDb/db";

    public static void main(String[] args) throws SQLException {
        DriverManager.getConnection(JDBC_H_2_LOCAL_DB_DB, "sa", "");
    }
}
