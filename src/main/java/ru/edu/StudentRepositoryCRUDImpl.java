package ru.edu;

import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class StudentRepositoryCRUDImpl implements StudentsRepositoryCRUD{

    public static final String INSERT_STUDENT = "INSERT INTO STUDENTS (id, first_name, last_name, birth_date, is_graduated) VALUES(?, ?, ?, ?, ?)";
    public static final String SELECT_BY_ID = "SELECT * FROM STUDENTS WHERE id = ?";
    private Connection connection;

    public StudentRepositoryCRUDImpl(Connection connection) {
        this.connection = connection;
    }

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student
     * @return сгенерированный UUID
     */
    @Override
    public UUID create(final Student student) {
        UUID id = UUID.randomUUID();
        int insertedRows = execute(INSERT_STUDENT, id.toString(), student.getFirstName(), student.getLastName(), student.getBirthDate(), student.isGraduated());

        if(insertedRows == 0){
            throw new IllegalStateException("Not inserted");
        }

        return id;
    }

    private int execute(String sql, Object ... values){

        try (PreparedStatement statement = connection.prepareStatement(sql)){
            for (int i = 0; i < values.length; i++) {
                statement.setObject(i+1, values[i]);
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    /**
     * Получение записи по id из БД.
     *
     * @param id
     * @return
     */
    @Override
    public Student selectById(UUID id) {
        List<Student> query = query(SELECT_BY_ID, id.toString());
        if(query.isEmpty()){
            return null;
        }
        return query.get(0);
    }

    private List<Student> query(String sql, Object ... values){
        try(PreparedStatement statement = connection.prepareStatement(sql)) {

            for (int i = 0; i < values.length; i++) {
                statement.setObject(i+1, values[i]);
            }
            ResultSet resultSet = statement.executeQuery();

            List<Student> result = new ArrayList<>();
            while (resultSet.next()){
                result.add(map(resultSet));
            }
            return result;

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    private Student map(ResultSet resultSet) throws SQLException {
        Student student = new Student();
        student.setId(UUID.fromString(resultSet.getString("id")));
        student.setFirstName(resultSet.getString("first_name"));
        return student;
    }


    /**
     * Получение всех записей из БД.
     *
     * @return
     */
    @Override
    public List<Student> selectAll() {
        return null;
    }

    /**
     * Обновление записи в БД.
     *
     * @param student
     * @return количество обновленных записей
     */
    @Override
    public int update(Student student) {
        return 0;
    }

    /**
     * Удаление указанных записей по id
     *
     * @param idList
     * @return количество удаленных записей
     */
    @Override
    public int remove(List<UUID> idList) {
        return 0;
    }
}
