package ru.edu;

import org.junit.Test;
import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.Instant;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static ru.edu.LocalDbCreation.JDBC_H_2_LOCAL_DB_DB;

public class StudentsRepositoryCRUDTest {


    @Test
    public void createTest() throws SQLException {
        StudentRepositoryCRUDImpl crud = new StudentRepositoryCRUDImpl(getConnection());

        Student student = new Student("first", "last", new Date(Instant.now().getEpochSecond()), false);

        UUID createdId = crud.create(student);

        Student newStudent = crud.selectById(createdId);

        assertEquals(student.getFirstName(), newStudent.getFirstName());
        assertEquals(createdId, newStudent.getId());
    }


    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_H_2_LOCAL_DB_DB, "sa", "");
    }
}