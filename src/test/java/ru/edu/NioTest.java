package ru.edu;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class NioTest {

    @Test
    public void test() throws Exception {
        Path targetPath = Paths.get("./target");
        Optional<Long> sum = Files.walk(targetPath)
                .filter(Files::isRegularFile)
                .map(this::getFileSize)
                .reduce(Long::sum);

        if (sum.isPresent()) {
            System.out.println("Sum is: " + sum.get());
        }
        //.forEach(path -> System.out.println(path.toString()));





    }

    @Test
    public void readTest() throws IOException {
        Path pomPath = Paths.get("./pom.xml");
        List<String> lines = Files.readAllLines(pomPath, StandardCharsets.UTF_8);

        System.out.println(lines);


        try (BufferedReader bufferedReader = Files.newBufferedReader(pomPath, StandardCharsets.UTF_8)){
            String line = null;
            while ((line = bufferedReader.readLine()) != null){
                System.out.println(bufferedReader.readLine());
            }
        }
    }


    private long getFileSize(Path path) {

        try {
            return Files.size(path);
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }

    }
}
